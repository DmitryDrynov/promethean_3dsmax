import array
import json
import MaxPlus
import time
import pymxs
import MaxPlus
from pymxs import runtime as rt
from itertools import *


def dump(text):
    try:
        with pymxs.mxstoken():
            pymxs.print_(str(text) + "\n", False, True)
    except:
        pass


def execute(command):
    time.sleep(0.1)
    dump("PrometheanAI: Execute command '%s' " % command)
    try:
        with pymxs.mxstoken():
            result = rt.execute(command)
    except Exception as e:
        result = "Exception Command: " + e.message + "\n"
    return result


def executeFunc(function, *args):
    time.sleep(0.1)
    try:
        with pymxs.mxstoken():
            if args:
                result = function(args)
            else:
                result = function()
    except Exception as e:
        result = "Exception Function: " + e.message + "\n"
    return result


def command_switch(client, meta_command_str):
    for command_str in meta_command_str.split('\n'):

        # for Windows
        command_str = command_str.replace("\r", "")

        if not command_str:  # if str is blank
            continue

        command_list = command_str.split(' ')
        command = command_list[0]
        msg = None  # return message

        # Get scene name or None
        if command == 'get_scene_name':
            msg = str(execute("maxFilePath + maxFileName")) or 'None'

        # Get selection objects in 3dsmax
        elif command == 'get_selection':
            execute("current_selection = getCurrentSelection()")
            msg = str(mxsToArray(rt.current_selection))

        # Get visible objects in 3dsmax viewport
        elif command == 'get_visible_static_mesh_actors':
            msg = str(executeFunc(getObjectsInView))

        # Get selected and visible objects in 3dsmax viewport
        elif command == 'get_selected_and_visible_static_mesh_actors':
            execute("current_selection = getCurrentSelection()")
            msg = str(mxsToArray(rt.current_selection)) + ' ' + str(executeFunc(getObjectsInView))

        elif command == 'get_location_data' and len(command_list) > 1:
            obj_names = command_list[1].split(',')
            data_dict = {x: list(execute('$\'%s\'.pos' % x)) for x in obj_names if execute('$\'%s\' != undefined' % x)}
            msg = json.dumps(data_dict)

        elif command == 'get_pivot_data' and len(command_list) > 1:
            obj_names = command_list[1].split(',')
            data_dict = {}
            for obj_name in obj_names:
                if execute('$\'%s\' != undefined' % obj_name):
                    size, rotation, pivot = executeFunc(getTransform, obj_name)
                    data_dict[obj_name] = list(pivot)
            msg = json.dumps(data_dict)

        elif command == 'get_transform_data' and len(command_list) > 1:
            obj_names = command_list[1].split(',')
            data_dict = {}
            for obj_name in obj_names:
                if execute('$\'%s\' != undefined' % obj_name):
                    transform_data = executeFunc(get_transform_data, obj_name)
                    data_dict[obj_name] = transform_data
                    msg = json.dumps(data_dict)

        elif command == 'add_objects' and len(command_list) > 1:
            """ add a group of objects and send back a dictionary of how their proper dcc names
            takes a json string as input that is a dictionary with old dcc name ask key and this dict as value 
            (asset_path, name, location, rotation, scale) """
            obj_list = json.loads(command_str.replace('add_objects ', ''))  # json str has spaces so doing this
            return_dict = {}

            for old_dcc_name in obj_list:
                if execute('$\'%s\' == undefined' % old_dcc_name):
                    return_dict[old_dcc_name] = executeFunc(add_object, obj_list[old_dcc_name])
                else:
                    dump('Adding object - %s already exists!' % old_dcc_name)
            msg = json.dumps(return_dict)

        elif command == 'add_objects_from_polygons' and len(command_list) > 1:
            obj_list = json.loads(command_str.replace('add_objects_from_polygons ', ''))  # str has spaces so doing this
            executeFunc(add_objects_from_polygons, obj_list)

        elif command == 'add_objects_from_triangles' and len(command_list) > 1:
            obj_list = json.loads(command_str.replace('add_objects_from_triangles', ''))  # str has spaces so doing this
            return_dict = executeFunc(add_objects_from_triangles, obj_list)
            msg = json.dumps(return_dict)  # return names of newly created objects

        elif command == 'parent' and len(command_list) > 1:
            children = command_list[1].split(',')
            executeFunc(valid_children, children)

        elif command == 'match_objects' and len(command_list) > 1:
            dcc_names = command_list[1].split(',')
            return_dict = {}
            transforms = [x for x in rt.objects if x.transform is not None]
            for dcc_name in dcc_names:
                for transform in transforms:
                    if transform.name == dcc_name:
                        return_dict[dcc_name] = transform.name
                        break
            msg = json.dumps(return_dict)

        elif command == 'isolate_selection':
            execute('actionMan.executeAction 0 "197"')

        elif command == 'learn_file' and len(command_list) > 1:  # open and learn a file
            learn_dict = json.loads(command_str.replace('learn_file ', ''))
            executeFunc(learn_file, learn_dict['file_path'], learn_dict['learn_file_path'], learn_dict['tags'],
                        learn_dict['project'])
            msg = json.dumps(True)  # return a message once it's done

        elif command == 'get_vertex_data_from_scene_object' and len(command_list) > 1:
            dcc_name = command_list[1]
            vert_list = executeFunc(get_triangle_positions, dcc_name,
                                    False)  # direction mask to adjust for thick wall objects
            vert_list = [{i: vert for i, vert in enumerate(
                vert_list)}]  # TODO this is to keep parity with unreal integration. should be fixed eventually
            vert_dict = {'vertex_positions': vert_list}
            msg = json.dumps(vert_list)
            dump(vert_dict)

        elif command == 'report_done':
            msg = json.dumps('Done')  # return a message once it's done

        elif command == 'screenshot' and len(command_list) > 1:
            dump('start')
            path = command_str.replace('screenshot ', '')  # in case there are spaces in the path
            path.strip()
            execute("max zoomext sel all")  # set all objects in scene
            execute("viewport.setType #view_persp_user")  # set perspective view
            execute("img = gw.getViewportDib()")
            execute("img.fileName = \"" + path + "\"")
            msg = str(execute("save img"))

        else:  # pass through - standalone sends actual DCC code
            execute(command_str)

        if msg is not None:
            dump("PrometheanAI: Outgoing message > '%s' " % msg)
            client.sendall(msg.encode())


def learn_file(args):
    file_path = args[0]
    learn_file_path = args[1]
    extra_tags = args[2] or []
    project = args[3] or None
    from_selection = args[4] or False

    # [open file] cmds.file(file_path, open=1, force=1)  # will wait to open
    learn(learn_file_path, extra_tags=extra_tags, project=project, from_selection=from_selection)


def learn(file_path, extra_tags=[], project=None, from_selection=False):
    pass


def learningCacheDataToFile(file_path, raw_data, scene_id, extra_tags=[], project=None):
    pass


def getRawObjectData(transform_node, predict_rotation=False):
    pass


def getAllObjectsRawData(predict_rotation=False, selection=False):
    pass


def valid_children(args):
    children = args[0]
    parent_name = children.pop(0)  # first one is the parent
    if execute('$\'%s\' != undefined' % parent_name):
        for x in children:
            if execute('$\'%s\' != undefined' % x):
                obj = rt.getNodeByName(x)
                parent_list = get_parent_list(obj, only_names=True)
                if parent_name not in parent_list:
                    dump('========== valid_children')
                    obj.parent = rt.getNodeByName(parent_name)
                    dump(x)


def getObjectsInView():
    objs_in_view = []

    mxsToArray(rt.objects)
    for obj in rt.objects:
        if isVisible(obj):
            objs_in_view.append(obj.name)

    return objs_in_view


def isVisible(obj):
    if cameraCull(obj.min) or cameraCull(obj.max) or cameraCull(obj.center):
        return True
    else:
        return False


def cameraCull(pos):
    p = pos * rt.viewport.getTM()
    viewSize = rt.getViewSize()

    start = rt.mapScreenToView(rt.Point2(0, 0), p[2])
    end = rt.mapScreenToView(rt.Point2(viewSize[0], viewSize[1]), p[2])
    norm = start - end
    s = rt.Point2((rt.renderWidth / abs(norm.x)) * (p.x - start.x), - (rt.renderHeight / abs(norm.y)) * (p.y - start.y))

    if s[0] > 0 and s[1] > 0 and s[0] < rt.renderWidth and s[1] < rt.renderHeight:
        return True
    else:
        return False


def mxsToArray(mxsWrapperArray):
    arr = []
    for el in mxsWrapperArray:
        arr.append(el.name)
    return arr


def getTransform(args):
    # get size, rotation, pivot
    # - unparent from existing parent

    obj = rt.getNodeByName(args[0])
    parent_name = get_parent_list(obj)
    temp_obj = obj
    old_parent = None

    # set parent object to World
    if len(parent_name):  # len is zero if parented to world
        old_parent = temp_obj.parent
        temp_obj.parent = None

    # - get transform data
    old_rotation = temp_obj.rotation
    old_pivot = temp_obj.pivot

    # set pivot to object center
    temp_obj.pivot = temp_obj.center

    # unlock rotation attributes in case locked
    rt.setTransformLockFlags(temp_obj, rt.BitArray(4, 5, 6))  # 4,5,6 is X,Y,Z rotation axes

    # rotate to [0,0,0]
    temp_obj.rotation = rt.eulerangles(0, 0, 0)

    # get transform data
    bbox = rt.nodeGetBoundingBox(temp_obj, temp_obj.transform)
    pivot = getPivot(bbox)
    size = getSize(bbox)

    # return original rotation and pivot position
    temp_obj.rotation = old_rotation  # set the rotation back
    temp_obj.pivot = old_pivot  # set the pivot back

    # return old parent
    if len(parent_name):
        temp_obj.parent = old_parent

    return size, old_rotation, pivot


def getPivot(bbox):
    # pivot is at the center of bounding box on XZ and is min on Y
    bbox_min = bbox[0]
    bbox_max = bbox[1]
    return [bbox_min[0] + (bbox_max[0] - bbox_min[0]) / 2, bbox_min[1] + (bbox_max[1] - bbox_min[1]) / 2,
            bbox_min[2] + (bbox_max[2] - bbox_min[2]) / 2]


def getSize(bbox):
    bbox_min = bbox[0]
    bbox_max = bbox[1]
    return [bbox_max[0] - bbox_min[0], bbox_max[1] - bbox_min[1], bbox_max[2] - bbox_min[2]]


def get_parent_list(obj, only_names=False):
    parent_list = []
    if obj.parent is not None:
        if only_names:
            parent_list.append(obj.parent.name)
        else:
            parent_list.append(obj.parent)
        parent_list.extend(get_parent_list(obj.parent, only_names))
        parent_list.reverse()
    return parent_list


def get_triangle_positions(args):
    obj = rt.getNodeByName(args[0])
    # direction_mask = args[1]
    # bbox = rt.nodeGetBoundingBox(obj, obj.transform)
    # bbox_center = getPivot(bbox)
    # dir_mask_threshold = 0.0  # dot product value
    face_verts, debug_faces, debug_positions = [], [], []
    res = polyTriangulate(obj)
    # face_num = obj.numFaces
    # for i in range(1, face_num):
    #     current_face_verts = []
    #     vert_ids = rt.polyOp.getFaceVerts(obj, i)
    #     for vert_id in vert_ids:
    #         pos = rt.polyOp.getVert(obj, vert_id)
    #         current_face_verts.append([pos.x, pos.y, pos.z])
    #     face_verts += current_face_verts
    return res


def polyTriangulate(obj):
    if obj is not None:
        rt.convertToPoly(obj)
        execute("select $" + obj.name)
        execute("max modify mode")
        # triMod = rt.Edit_Poly()
        # triMod.name = "Triangulate"
        # rt.addModifier(obj, triMod)
        # rt.modPanel.setCurrentObject(triMod)
        rt.subObjectLevel = 1
        execute("actionMan.executeAction 0 \"40021\"")
        execute("$.EditablePoly.ConnectVertices ()")
        # triMod.ButtonOp("ConnectVertices")
        execute("deselect $" + obj.name)
        return True
    else:
        return False


def get_transform_data(args):
    obj = rt.getNodeByName(args[0])
    return {'translation': str(obj.transform.translation), 'rotation': str(obj.transform.rotation),
            'scale': str(obj.transform.scale)}


# add_objects {"Group111": {"group": true, "name": "Group111", "location": [0,1,2], "rotation":[3,4,5], "scale":[1,1,1]}}
# add_objects {"NewMesh111": {"group": false, "name": "NewMesh111", "location": [10,11,12], "rotation":[3,4,5], "scale":[1,2,3]}}
def add_object(args, obj_dict=None):
    obj_dict = args[0]
    new_obj = None

    if obj_dict['group']:
        dummy = rt.Dummy()
        dummy.name = "dummy_" + obj_dict['name']
        new_obj = rt.group(dummy)
        new_obj.name = obj_dict['name']
    elif "asset_path" in obj_dict:
        new_obj = rt.Box()
        new_obj.name = obj_dict['name']
        new_obj.position.z = -0.5
    else:
        new_obj = rt.Box()
        new_obj.name = obj_dict['name']
        new_obj.position.z = -0.5

    new_obj.scale = rt.Point3(obj_dict['scale'][0], obj_dict['scale'][1], obj_dict['scale'][2])
    new_obj.position = rt.Point3(obj_dict['location'][0], obj_dict['location'][1], obj_dict['location'][2])
    new_obj.rotation = rt.eulerangles(obj_dict['rotation'][0], obj_dict['rotation'][1], obj_dict['rotation'][2])

    return new_obj.name


# add_objects_from_polygons {"name": "FixedFurnitureCoatCloset", "points": [[0.0, 0.0, 0.0], [103.69, 0.0, 0.0], [103.69, 0.0, 60.0], [0.0, 0.0, 60.0]], "transform": {"translation": [937.9074, 0.0, 192.8589], "rotation": [0,0,0], "scale": [1,1,1]}}
# add_objects_from_polygons {"name": "FixedFurnitureCoatCloset", "points": [[0.0, 0.0, 0.0], [103.69, 0.0, 0.0], [103.69, 0.0, 60.0], [0.0, 0.0, 60.0]]}
def add_objects_from_polygons(args):
    """ each item in the list is a dictionary that stores a polygon based object
        TODO: a single object is currently one polygon. Need multi-polygon objects
     {'name': 'FixedFurniture CoatCloset',
      'points': [(0.0, 0.0, 0.0),
                 (103.69, 0.0, 0.0),
                 (103.69, 0.0, 60.0),
                 (0.0, 0.0, 60.0)],
      'transform': { 'translation': [0,0,0], 'rotation': [0,0,0], 'scale': [0,0,0] } """
    geometry_list = args

    for geometry_dict in geometry_list:
        vert_array = geometry_dict["points"]

        new_mesh = build_mesh_from_verts(geometry_dict["name"], vert_array)

        if 'transform' in geometry_dict:  # only furniture has a transform so far
            position = geometry_dict['transform']['translation']
            rotation = geometry_dict['transform']['rotation']
            scale = geometry_dict['transform']['scale']
            setTransform(new_mesh, position, rotation, scale)


# add_objects_from_triangles {"obj1": {"name": "obj1", "verts": [[0.0,0.0,0.0], [1.0,0.0,0.0], [1.0,1.0,0.0], [0.0,1.0,0.0], [1.0,-1.0,0.0], [0.0,-1.0,0.0]], "tri_ids": [[0,1,2], [0,2,3], [0,5,1], [1,5,4]], "normals": [[0.0,1.0,0.0],[0.0,1.0,0.0],[0.0,1.0,0.0],[0.0,1.0,0.0]], "transform": {"translation": [0.0, 0.0, 0.0], "rotation": [15,30,45], "scale": [1,2,3]}}}
# add_objects_from_triangles {"obj1": {"name": "obj1", "verts": [[0.0,0.0,0.0], [1.0,0.0,0.0], [1.0,1.0,0.0], [0.0,1.0,0.0], [1.0,-1.0,0.0], [0.0,-1.0,0.0]], "tri_ids": [[0,1,2], [0,2,3], [0,1,5], [1,5,4]], "normals": [[0.0,1.0,0.0],[0.0,1.0,0.0],[0.0,1.0,0.0],[0.0,1.0,0.0]]}}
def add_objects_from_triangles(args):
    """ input is a dictionary with a unique dcc_name for a key and a dictionary that stores a triangle-based object value
    { dcc_name:
     {'name': 'FixedFurniture CoatCloset',
      'tri_ids': [(0, 1, 2),(0, 2, 3), (0, 1, 4) ... ],
      'verts': [(0.0, 0.0, 0.0), (103.69, 0.0, 0.0), (103.69, 0.0, 60.0), (), (), ... ], - unique vertexes
      'normals': [(0.0, 1.0, 0.0), (0.0, 1.0, 0.0), (0.0, 1.0, 0.0), (), (), ... ], - normal per tri, matching order
      'transform': { 'translation': [0,0,0], 'rotation': [0,0,0], 'scale': [0,0,0] },
      dcc_name: {}
    } """
    geometry_dicts = args[0]
    out_names = {}
    for dcc_name in geometry_dicts:
        geometry_dict = geometry_dicts[dcc_name]
        temp_new_objects = []
        unique_verts = geometry_dict['verts']
        print('Constructing %s' % geometry_dict['name'])
        for i, tri_vert_id in enumerate(geometry_dict['tri_ids']):
            tri = [toPoint3(unique_verts[tri_vert_id[0]]), toPoint3(unique_verts[tri_vert_id[1]]),
                   toPoint3(unique_verts[tri_vert_id[2]])]
            tri = check_winding_order(tri, geometry_dict['normals'][i])
            temp_new_objects.append(polyCreateFacet(tri))
        new_mesh = polyUnite(geometry_dict['name'], temp_new_objects)
        rt.meshop.weldVertsByThreshold(new_mesh, new_mesh.verts, 0.1)  # merge verts with threshold distance at 0.1
        rt.CenterPivot(new_mesh)
        if 'transform' in geometry_dict:  # only furniture has a transform so far
            position = geometry_dict['transform']['translation']
            rotation = geometry_dict['transform']['rotation']
            scale = geometry_dict['transform']['scale']
            setTransform(new_mesh, position, rotation, scale)

        out_names[dcc_name] = geometry_dict['name']
    return out_names


def toPoint3(any_array):
    return rt.Point3(any_array[0], any_array[1], any_array[2])


def polyUnite(name, objects_array):
    final_obj = objects_array[0]
    final_obj.name = name
    for i in range(1, len(objects_array)):
        rt.meshop.attach(final_obj, objects_array[i])
    return final_obj


def polyCreateFacet(tri):
    mesh = rt.editable_mesh()
    rt.meshop.setNumVerts(mesh, 3)
    rt.meshop.setvert(mesh, [1, 2, 3], tri)
    rt.meshop.createPolygon(mesh, [1, 2, 3])
    return mesh


def build_mesh_from_verts(name, vert_array, face_verts=4):
    vert_count = len(vert_array)
    rt.execute("tmp_mesh = mesh numverts:" + str(vert_count) + " name:\"" + name + "\"")
    for v in range(0, vert_count):
        vert = vert_array[v]
        rt.meshop.setvert(rt.tmp_mesh, v + 1, rt.Point3(vert[0], vert[1], vert[2]))

    for v in range(0, vert_count / face_verts, face_verts):
        if face_verts == 4:
            face_indx = [v + 1, v + 2, v + 3, v + 4]
        else:
            face_indx = [v + 1, v + 2, v + 3]
        rt.meshop.createPolygon(rt.tmp_mesh, face_indx)

    return rt.tmp_mesh


def check_winding_order(tri, normal):
    """ making sure triangles face the intended way """
    ab = [a - b for a, b in zip(tri[0], tri[1])]
    ac = [a - b for a, b in zip(tri[0], tri[2])]
    cross_product = cross(ab, ac)
    if dot(normal, cross_product) < 0:
        tri.reverse()
    return tri


def cross(a, b):
    """ cross product, maya doesn't have numpy """
    c = [a[1] * b[2] - a[2] * b[1],
         a[2] * b[0] - a[0] * b[2],
         a[0] * b[1] - a[1] * b[0]]
    return c


def dot(a, b):
    """ dimension agnostic dot product """
    return sum([x * y for x, y in zip(a, b)])


def setTransform(mesh, position, rotation, scale):
    mesh.position = rt.Point3(position[0], position[1], position[2])
    mesh.rotation = rt.eulerangles(rotation[0], rotation[1], rotation[2])
    mesh.scale = rt.Point3(scale[0], scale[1], scale[2])
